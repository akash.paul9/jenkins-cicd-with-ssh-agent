# Jenkins CICD with SSH Agent

Installed the following docker-compose file with sudo docker-compose.yml up –d command.  

Hit localhost:8080 in browser and setup jenkins. Make a project with pipeline and give all github credentials. Now our target is deploy a docker compose file to different machine with ssh agent, So we need to add credentials of the remote machine. Go to Manage Jenkins -> Manage Credentials -> Add Credentials. Fill up the form. 

Save it and build the project. It will show a error that Host name couldn't resolve. So make ssh key of the container and add that machines authorized keys. TRy to log in from the container. if you miss it, you can add ssh appmin@192.168.1.237 -o StrictHostKeyChecking=no. It will directly log into the machine as it is not highly recommanded.  
